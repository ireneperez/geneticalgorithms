#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  9 19:35:28 2018

@author: irene
"""
import numpy as np
import copy
import matplotlib.pyplot as plt

# Parameters come from the file: Parameters
with open('Parameters') as f:
    for line in f:
        exec(line)

# computing time         
import time
start = time.time()

###### POPULATION #######
#n_individuals = 50 
#n_generations = 5000

###### REPRESENTATIONS #######
#n_cromosomes = 10 # n
#n_genes = 8 # number of bits in a genotype
#the number of genes determines which integer can we get, and determines de n_distance 
real_values = np.linspace(l_lim,u_lim,n_distance)
def random_bin(n_genes):
    x_i_bin = np.random.uniform(low=0.0, high=1.0, size=n_genes)
    x_i_bin = list(x_i_bin)
    for genes in x_i_bin:
        if (genes < 0.5): x_i_bin[x_i_bin.index(genes)] = 0
        else: x_i_bin[x_i_bin.index(genes)] = 1
    return x_i_bin

###### FITNESS FUNCTIONS #######
def fitness_esfera(x_i):
# problemas sencillos --> mas explotacion
    result = 0
    for item in x_i:
        item = item*item
        result = result+item
    return result
def fitness_schwefel(x_i):
# problemas complejos --> mas exploracion
    d = len(x_i)
    suma = 0
    for item in x_i:
        prod = item*np.sin(np.sqrt(abs(item)))
        suma = suma+prod
    result = 418.9829*d - suma    
    return result

######### INITIALIZE POPULATION #############
population = []
population_indexes = []
for individual in range(n_individuals):
    feno = []
    fenotype_indexes = []
    for x_i in range(n_cromosomes):
        x_i_bin = random_bin(n_genes)
        x_i_pos = np.packbits(x_i_bin,axis=0)
        genotype = real_values[x_i_pos]
        feno.append(genotype[0]) 
        fenotype_indexes.append(x_i_pos[0])      
    population.append(feno)   
    population_indexes.append(fenotype_indexes)
    fitnesses = []
    for fenotype in population:
        fitnesses.append(fitness_esfera(fenotype))
    new_population_indexes = copy.deepcopy(population_indexes)
    new_population = copy.deepcopy(population)

#print("Population initialized: ",population)
vector1 = []
vector2 = []
for generation in range(n_generations):
    print("GENERATION NUMBER ",generation)
    ####### PARENT SELECTION ################
    fitness_list = copy.deepcopy(fitnesses)
    population_indexes = copy.deepcopy(new_population_indexes)
    population = copy.deepcopy(new_population)
    #print("The parent fitness list is: ",fitness_list)
    fitness_list_ranked = sorted(fitness_list,key=float,reverse=False),list(range(len(fitness_list),0,-1)),[0.01,0.2,0.5,0.7,0.85,1]
    fitness_list_sorted = fitness_list_ranked[0]
    parents = []
    for fitness_value in fitness_list_sorted:
        parents.append([(fitness_list.index(fitness_value))+1,fitness_value,population_indexes[fitness_list.index(fitness_value)]])

    all_childs = []
    count_parents = 0
    while count_parents<= (int(len(parents))//2 +1):
        selected_parents = parents[count_parents:count_parents+2]
        count_parents+=2    
    
        ###### RECOMBINATION ###################
        recombination = False
        random_variable = np.random.uniform(low=0.0, high=1.0, size=1)
        if (random_variable<=p_crossover): recombination = True
        else: recombination = False #asexual copy of parents
        cuts = np.random.permutation(np.arange(1,n_genes-1))[:2]# 2-point crossover
        cuts = sorted(cuts,key=int) #each genotype will be cut at the same points
        offspring = []
        for parent in selected_parents:
            fenotype1 = []
            for cromosome_number in range(n_cromosomes):
                fenotype1.append(list(np.unpackbits(np.array(parent[2][cromosome_number],dtype=np.uint8),axis=0)))
            for cromo in fenotype1:
                offspring.append([cromo[0:cuts[0]],cromo[cuts[0]:cuts[1]],cromo[cuts[1]:]])
        if(recombination):
            child_1 = []
            child_2 = []
            for genotype in range(n_cromosomes):
                child_1.append([offspring[genotype][0],offspring[n_cromosomes+genotype][1],offspring[genotype][2]])
                child_2.append([offspring[n_cromosomes+genotype][0],offspring[genotype][1],offspring[n_cromosomes+genotype][2]])
        else:
            child_1 = offspring[0:n_cromosomes]
            child_2 = offspring[n_cromosomes:] 
        child1 = []
        for cromosomes in child_1:
            extra = []
            for segments in cromosomes:
                extra += segments
            child1+=extra
        child2 = []
        for cromosomes in child_2:
            extra = []
            for segments in cromosomes:
                extra += segments
            child2 += extra
        childs = child1,child2

        ####### MUTATION #####################
        mutation = False
        new_childs = []
        for child in childs:
            count = 0
            i = 0
            for binaries in child:
                random_variable = np.random.uniform(low=0.0, high=1.0, size=1)
                if (random_variable<=p_mutation):
                    mutation = True
                    count += 1
                    child[i] = 1-binaries
                else: mutation = False
                i+=1
            child = [child[j:j+n_genes] for j in range(0,len(child),n_genes)]     
            child_values = []
            for chromosome in child:
                child_values.append(np.packbits(chromosome,axis=0)[0])
            new_childs.append(child_values)
        all_childs.extend(new_childs)
    number_childs = len(all_childs)
    
    ######## SURVIVOR SELECTIONS ################
    current_population = population_indexes+all_childs #childs always go at the end of the list
    current_population_indexes = copy.deepcopy(current_population)
    #print("Current population ",current_population,current_population_indexes)
    new_fitness_list = []
    for fenotype in current_population:
        new_vec = [0]*len(fenotype)
        for value in fenotype:
            new_vec[fenotype.index(value)] = real_values[int(value)] 
        new_fitness_list.append(fitness_esfera(new_vec))
    # applying elitism
    best_parent = min(new_fitness_list[0:-number_childs])
    #print("Best parent's fitness: ",best_parent)
    new_fitness_list_to_sort = []
    for fitness_childs in new_fitness_list[number_childs:]:
        if(best_parent<=fitness_childs):
            new_fitness_list_to_sort = new_fitness_list[number_childs:]+[new_fitness_list[new_fitness_list.index(best_parent)]]
        else: new_fitness_list_to_sort = copy.deepcopy(new_fitness_list)
        break
    new_fitness_list_sorted = sorted(new_fitness_list_to_sort,key=float,reverse=False)
    new_fitness_list_sorted = new_fitness_list_sorted[0:n_individuals]
    actual_population = []
    for fitness_value in new_fitness_list_sorted:
        actual_population.append([(new_fitness_list.index(fitness_value))+1,fitness_value,current_population_indexes[new_fitness_list.index(fitness_value)],current_population[new_fitness_list.index(fitness_value)]])
    #print("New population achieved: ",actual_population)

    #### we are ready to use the new population found
    new_population_indexes = []
    new_population = []
    fitnesses = []
    for individuals in actual_population:  
        new_population_indexes.append(individuals[2])
        new_population.append(individuals[3])
        fitnesses.append(individuals[1])

    vector1.append(generation)
    vector2.append(min(fitnesses))
    
# time computing
end = time.time()
print("Time elapsed: ",end - start)
vector1 = np.array(vector1,dtype=np.float64)
vector2 = np.array(vector2,dtype=np.float64)
#plotting
plt.figure()
plt.plot(vector1,vector2,label='esfera')
plt.xlabel('Generaciones')
plt.ylabel('Minimo valor de fitness')
plt.title('Evolucion del valor de la funcion esfera')
plt.legend(loc='upper right')
plt.show()
# saving in file
new_f = open('Results0025.txt','w')
for i in range(len(vector1)):
    new_f.write("%i %5.2f\n" % (vector1[i], vector2[i]))
new_f.close()