#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 19:47:09 2019
@author: irene
"""
import numpy as np
import sys, copy, re, random, math, operator
import matplotlib.pyplot as plt

# Computing time
import time
start = time.time()

######### PARAMETERS ################
# Parameters come from the file: Parameters_3
with open('Parameters_3') as f:
    for line in f:
        exec(line)

######### GRAMMATICAL EVOLUTION #######################
class Grammar(object):
    """ Context Free Grammar """
    NT = "NT" # Non Terminal
    T = "T" # Terminal

    def __init__(self, file_name):
        if file_name.endswith("pybnf"):
            self.python_mode = True
        else:
            self.python_mode = False
        self.rules = {}
        self.non_terminals, self.terminals = set(), set()
        self.start_rule = None

        self.read_bnf_file(file_name)

    def read_bnf_file(self, file_name):
        """Read a grammar file in BNF format"""
        # <.+?> Non greedy match of anything between brackets
        non_terminal_pattern = "<.+?>"
        rule_separator = "::="
        production_separator = "|"

        # Read the grammar file
        for line in open(file_name, 'r'):
            if not line.startswith("#") and line.strip() != "":
                # Split rules. Everything must be on one line
                if line.find(rule_separator):
                    lhs, productions = line.split(rule_separator)
                    lhs = lhs.strip()
                    if not re.search(non_terminal_pattern, lhs):
                        raise ValueError("lhs is not a NT:", lhs)
                    self.non_terminals.add(lhs)
                    if self.start_rule == None:
                        self.start_rule = (lhs, self.NT)
                    # Find terminals
                    tmp_productions = []
                    for production in [production.strip()
                                       for production in
                                       productions.split(production_separator)]:
                        tmp_production = []
                        if not re.search(non_terminal_pattern, production):
                            self.terminals.add(production)
                            tmp_production.append((production, self.T))
                        else:
                            # Match non terminal or terminal pattern
                            # TODO does this handle quoted NT symbols?
                            for value in re.findall("<.+?>|[^<>]*", production):
                                if value != '':
                                    # Anem a fer una ñapa
                                    if '(' in value: 
                                        value = value.strip('(')
                                        symbol = (value, self.T)
                                        self.terminals.add(value)
                                        tmp_production.append(symbol)
                                        if '(' not in self.terminals:
                                            self.terminals.add('(')
                                        tmp_production.append(('(',self.T))
                                    else:
                                        if not re.search(non_terminal_pattern,
                                                        value):
                                            symbol = (value, self.T)

                                            self.terminals.add(value)
                                        else:
                                            symbol = (value, self.NT)
                                        tmp_production.append(symbol)
                        tmp_productions.append(tmp_production)
                    # Create a rule
                    if not lhs in self.rules:
                        self.rules[lhs] = tmp_productions
                    else:
                        raise ValueError("lhs should be unique", lhs)
                else:
                    raise ValueError("Each rule must be on one line")

    def __str__(self):
        return "%s %s %s %s" % (self.terminals, self.non_terminals,
                                self.rules, self.start_rule)

    def generate(self, _input, max_wraps):
        """Map input via rules to output. Returns output and used_input"""
        used_input = 0
        wraps = 0
        output = []
        production_choices = []

        unexpanded_symbols = [self.start_rule]
        while (wraps < max_wraps) and (len(unexpanded_symbols) > 0):
            # Wrap
            if used_input % len(_input) == 0 and \
                    used_input > 0 and \
                    len(production_choices) > 1:
                wraps += 1
            # Expand a production
            current_symbol = unexpanded_symbols.pop(0)            
            # print('Unexpanded  ',wraps,output, "   length  ",len(unexpanded_symbols))
            # Set output if it is a terminal
            if current_symbol[1] != self.NT:
                output.append(current_symbol[0])
            else:
                production_choices = self.rules[current_symbol[0]]
                # Select a production
                current_production = _input[used_input % len(_input)] % len(production_choices)
                # Use an input if there was more then 1 choice
                if len(production_choices) > 1:
                    used_input += 1
                # Derviation order is left to right(depth-first)
                unexpanded_symbols = production_choices[current_production] + unexpanded_symbols

        #Not completly expanded
        if len(unexpanded_symbols) > 0:
            return (None, used_input)

        output = "".join(output)
        return (output, used_input)


class Individual(object):
    """A GE individual"""
    def __init__(self, genome,values):
        if genome == None:
            self.genome = [random.randint(0,CODONS_SIZE)
                           for _ in range(random.randint(15,D_max/2))]
        else:
            self.genome = genome
        self.fitness = -1000000000
        self.phenotype = None
        self.used_codons = 0
        self.f, self.faprox = [],[]

        if values == None:
            self.values = interval(M,lim_i,lim_s)
        else: self.values = values

    def __str__(self):
        return ("Individual: " +
                str(self.phenotype) + "; " + str(self.fitness))

    def error_calc(self):
        e = 0
        self.f,self.faprox = [],[]
        frase = str(self.phenotype)
        # print('Frase: ',self.genome,frase)
        for xs in self.values:
            faprox = 0
            for term in frase.split(')')[:-1]:
                num1 = expo(term[:7])
                if term[8] == 'P': num2 = K_P(expo_s(term[10:16]),expo_s(term[16:22]),float(term[22]),xs)
                elif term[8] == 'G': num2 = K_G(expo_s(term[10:16]),expo_s(term[16:22]),xs)
                elif term[8] == 'S': num2 = K_S(expo_s(term[10:16]),expo_s(term[16:22]),xs)
                faprox += num1*num2
            f = function_0(xs)
            # Evaluation function
            U,K0,K1 = 0.1,1,10
            if np.abs(f-faprox) <= U: w = K0
            else: w = K1
            e += w*(np.abs(f-faprox))
            self.f.append(f)
            self.faprox.append(faprox)
        e = e/M
        self.fitness = e


######### AUXILIAR FUNCTIONS ############
def interval(M,lim_i,lim_s):
    x_i = np.linspace(lim_i,lim_s,M)
    x_i = np.array(x_i)
    return x_i

def K_G (gamma,c,x):
    return np.exp(-gamma*(c-x)**2)

def K_P (alpha,beta,d,x):
    return (alpha*x+beta)**d

def K_S (delta,theta,x):
    return np.tanh(delta*x+theta)

def expo(input_string):
    # print(input_string[:4],input_string[5:7])
    expo_num = float(input_string[:4])*(10**(float(input_string[5:7])))
    return expo_num
def expo_s(input_string):
    # print(input_string[:3],input_string[4:6])
    expo_num = float(input_string[:3])*(10**(float(input_string[4:6])))
    return expo_num

def function_0 (x):
    return 8*np.exp(-2*(x-2)**2) + (2*x+1) + 3*np.tanh(3*x+2)


def print_stats(generation, individuals):
    """Print the statistics for the generation and individuals"""
    def ave(values):
        """ Return the average of the values """
        return float(sum(values))/len(values)
    def std(values, ave):
        """ Return the standard deviation of the values and average """
        return math.sqrt(float(sum((value-ave)**2 for value in values))/len(values))

    valid_inds = [i for i in individuals if i.phenotype is not None]
    if len(valid_inds) == 0:
        fitness_vals = [0]
        used_codon_vals = [0]
    else:
        fitness_vals = [i.fitness for i in valid_inds]
        used_codon_vals = [i.used_codons for i in valid_inds]
    ave_fit = ave(fitness_vals)
    std_fit = std(fitness_vals,ave_fit)
    ave_used_codons = ave(used_codon_vals)
    std_used_codons = std(used_codon_vals, ave_used_codons)
    print("Gen:%d evals:%d ave:%.2f+-%.3f aveUsedC:%.2f+-%.3f %s" % (
            generation, (GENERATIONS*generation), ave_fit, std_fit,
            ave_used_codons, std_used_codons, individuals[0]))
    return ave_fit

def evaluate_fitness(individuals, grammar):
    """ Perform the mapping for each individual """
    for ind in individuals:
        ind.phenotype, ind.used_codons = grammar.generate(ind.genome,MAX_WRAPS)
        # print("Used codons:   ",ind.used_codons," genome: ",len(ind.genome)," phenotype:   ",ind.phenotype)
        if ind.phenotype is not None:
            ind.error_calc()

######### INITIALIZE POPULATION ##########
def initialise_population(size):
    """Create a population of size and return"""
    return [Individual(None,None) for _ in range(size)]

######### SELECT PARENTS #################
def tournament_selection(population):
    x = 32
    x = (x*len(population))/100
    if x< 1: x=1
    winners = population[:x]
    losers = population[x:]
    tournament_size = PARENTS_SIZE # how many parents do we want
    t_s = int(round(0.8*tournament_size))

    winners = random.sample(winners,t_s)
    losers = random.sample(losers,tournament_size-t_s)

    return winners+losers

########## RECOMBINATION #################
def check_length(c1,c2,parents):
    if len(c1)>D_max: c1 = parents[random.randint(0,1)].genome
    if len(c2)>D_max: c2 = parents[random.randint(0,1)].genome
    return c1,c2
        

def onepoint_crossover(p1,p2):
    # Generate crossover points from the parents
    cod_decode = 15
    max_p1, max_p2 = len(p1.genome), len(p2.genome)
    cp_p1,cp_p2 = random.randint(1,max_p1/cod_decode), random.randint(1,max_p2/cod_decode)
    cp_p1,cp_p2 = cp_p1*cod_decode,cp_p2*cod_decode

    # Make new chromosomes by crossover
    c1,c2 = [],[]
    if random.random() < CROSSOVER_PROBABILITY:
        c1 = p1.genome[:cp_p1] + p2.genome[cp_p2:]
        c2 = p2.genome[:cp_p2] + p1.genome[cp_p1:]
    else:
        c1, c2 = p1.genome[:], p2.genome[:]
    # If one of the children has len(genome)>D_max, it is substitued by one of the parents
    # c1,c2 = check_length(c1,c2,[p1,p2])
    # Assign them to new individuals
    return [Individual(c1,None), Individual(c2,None)]

########## MUTATION ######################
def mutation(ind):
    # One subtree has to replace the orginial subtree
    if random.random() < MUTATION_PROBABILITY:
        size = random.randint(1,D_max/15)
        new_tree = [random.randint(0,CODONS_SIZE) for i in range(size*15)]
        # Select the node to append the new subtree
        if len(ind.genome)>15: index = random.randint(1,len(ind.genome)/15)
        else: index = 1
        ind.genome = ind.genome[:index*15] + new_tree
    return ind

########## SELECTION OF SURVIVORS ########
def selection_survivors(new_pop,population):
    population.sort(key=lambda x: abs(x.fitness), reverse=False)

    for ind in population[:ELITE_SIZE]:
        new_pop.append(copy.copy(ind))
    new_pop.sort(key=lambda x: abs(x.fitness), reverse=False)

    return new_pop[:POPULATION_SIZE]

######### PERFORM STEPS ##################
def step(population,bnf_grammar,generation):
    # Select parents
    parents = tournament_selection(population)
    # Crossover parents
    new_pop = []
    while len(new_pop) < CHILDREN_SIZE:
        num = random.sample(parents,2)
        for p in num: parents.remove(p)
        for i in range(0,CHILDREN_SIZE/PARENTS_SIZE):
            new_pop.extend(onepoint_crossover(*num))
    # Mutate the individuals
    new_pop = list(map(mutation,new_pop))
    # Evaluate the fitness of the new population
    evaluate_fitness(new_pop,bnf_grammar)    
    # Replace the individuals 
    population = selection_survivors(new_pop,population)
    return population

##################### MAIN PROGRAM ###########################

def main(v1,v3):
    """ Run program """
    bnf_grammar = Grammar(GRAMMAR_FILE)
    # Create individuals
    population = initialise_population(POPULATION_SIZE)
    print('POPULATION CREATED')
    # Evaluate initial population
    evaluate_fitness(population,bnf_grammar)
    # Sort population by fitness
    population.sort(key=lambda x: abs(x.fitness), reverse=False)

    for generation in range(2,(GENERATIONS+1)):
        population = step(population,bnf_grammar,generation)
        print_stats(generation, population)

        v1.append(generation)
        v3.append(population[0].fitness)
    
    return v1,v3

v1,v3 = [],[]
v1,v3 = main(v1,v3)

# computing time
end = time.time()
print('Time elapsed : ',end-start)
# plotting
v1 = np.array(v1,dtype=np.float64)
v3 = np.array(v3,dtype=np.float64)
plt.figure()
plt.plot(v1,v3,label='Best fitness')
# plt.plot(v1,v2,label='Average fitness')
plt.xlabel('Numero de generaciones')
plt.ylabel('Mejor individuo')
plt.title('Evolucion de la aproximacion mediante un algoritmo GE')
plt.legend(loc='upper right')
plt.xlim([0,100])
# plt.ylim([0,10000000])
plt.show()

# saving in file
new_f = open('Results.txt','w')
for i in range(len(v1)):
    new_f.write("%i %5.2f\n" % (v1[i], v3[i]))
new_f.close() 