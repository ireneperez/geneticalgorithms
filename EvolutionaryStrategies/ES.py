#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 19:47:09 2019
@author: irene
"""
import numpy as np
import copy
import matplotlib.pyplot as plt
import operator

######### PARAMETERS ################
# Parameters come from the file: Parameters
with open('Parameters') as f:
    for line in f:
        exec(line)

class Individual():
    def __init__(self):
        self.size = M
        self.stepsize = 0
        self.values = []
        self.parameters = []
        self.thau = learning_rate # learning rate
        self.thaup = 0.001
        self.epsi = epsilon
        self.omega = 1
        self.c = 2
        self.gamma = 3
        self.error = 10000    
    def __repr__(self):
        return repr((self.values,self.stepsize,self.parameters,self.error))        
    def genotype(self):
        return np.concatenate((self.values,self.stepsize),axis=0)
    def error_calc(self,M):
        e = 0
        for xs in self.values:
            faprox = 0
            for n in range(0,n_parameters,n_variables):
                omega = self.parameters[n]
                c = self.parameters[n+1]
                gamma = self.parameters[n+2]
                faprox += omega*np.exp(-gamma*(c-xs)**2)
            f = 2*np.exp(-2*(xs-1)**2) - np.exp(-(xs-1)**2)
         # Evaluation function
            U = 0.1
            K0 = 1
            K1 = 10
            if np.abs(f-faprox) <= U: w = K0
            else: w = K1
            e += w*(np.abs(f-faprox))
        e = e/M
        self.error = e
        
    def mutation(self):
        # Uncorrelated mutation with one step size
        # New value for the step size
        rand = np.random.normal(0,1,1)
        new_stepsize = [stepi*np.exp(self.thau * rand) for stepi in self.stepsize]
        if new_stepsize < self.epsi: new_stepsize = self.epsi
        # New values for the vector
        new_p = [p_i + new_stepsize*np.random.normal(0,1,1) for p_i in self.parameters] 
        self.stepsize = new_stepsize
        self.parameters = np.concatenate(new_p,axis=0)    
        return 0
    
########## AUXILIAR FUNCTIONS ##############
def parent_selection(n_individuals):
    index1,index2 = np.random.choice(n_individuals,size=2,replace=False)
    return index1,index2

def recombination(M,indi1,indi2,n_stepsizes):
    # local intermediary recombination
    z = []
    for i in range(n_parameters):
        x_i = indi1.parameters[i]
        y_i = indi2.parameters[i]
        z_i = (x_i+y_i)/2
        z.append(np.array(z_i)) 
    for j in range(n_stepsizes):
        x_i = indi1.stepsize[j]
        y_i = indi2.stepsize[j]
        z_i = (x_i+y_i)/2
        z.append(np.array(z_i)) 
    return np.hstack(z)

def random_gene(M):
    x_i = np.linspace(lim_i,lim_s,M)
    x_i = np.array(x_i)
    return x_i


######### INITIALIZE POPULATION ##########
population = []
for i in range(n_individuals):
    indi = Individual()
    indi.values = random_gene(M)
    indi.stepsize = np.random.normal(0.0,1.0,size=n_stepsizes)
    indi.parameters = np.random.uniform(0.0,1.0,size=n_parameters)
    population.append(indi)
print("POPULATION CREATED")

######## RUN GENERATIONS ################
for generation in range(n_generations):
    list_offspring = []
    for i in range(n_offspring):
        # Select randomly two parents from the present population
        i1,i2 = parent_selection(n_individuals)
        # Recombiante these two parents to create one offspring
        new_individual = recombination(M,population[i1],population[i2],n_stepsizes)
        # Create a new individual from the result of the recombination
        offi = Individual()
        offi.values = population[0].values
        offi.stepsize = np.array([new_individual[-n_stepsizes:]])
        offi.parameters = new_individual[0:n_parameters]
        # Add the individual to the population of offspring
        list_offspring.append(offi)
        # Mutate this new individual
        offi.mutation()
        # Calculate the error between the original function and the approximation of this individual 
        offi.error_calc(M)
    
    # Apply survivor selection from parents and offspring (mu+lambda)
    for parent in population:
        parent.error_calc(M)
    list_offspring = list_offspring + population  
    
    # Sort the population by its error value
    list_offspring = sorted(list_offspring, key=lambda offi: offi.error,reverse=False)
    
    population = []
    # Keep in the population the best 20 individuals 
    population = list_offspring[:n_individuals]
    
    min_error = 1000
    for j in population:
        if min_error >= j.error: 
            min_error = j.error
            j_min = j
        
    print("Generation number ",generation," with error: ",min_error)